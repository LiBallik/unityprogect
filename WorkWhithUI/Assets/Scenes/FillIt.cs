﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class FillIt : MonoBehaviour
{
    public Image Kvadric;
    public Image Krug;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void setValue(Slider slider) {
        AudioListener.volume=slider.value;
        Kvadric.fillAmount = slider.value;
        Krug.fillAmount = slider.value;

    
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetAudio(Toggle audioToggle)
    {
        if (audioToggle.isOn)
        {
            AudioListener.volume = 1;
        }
        else
        {
            AudioListener.volume = 0;
        }
    }
}

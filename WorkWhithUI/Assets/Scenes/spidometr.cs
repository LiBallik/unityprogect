﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spidometr : MonoBehaviour
{
    public Image kolzo;
   
    // Start is called before the first frame update
    void Start()
    {
        kolzo.fillAmount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            if (kolzo.fillAmount < 0.776f)
            {
                kolzo.fillAmount += 0.004f;
            }
        }
        else
        {
            kolzo.fillAmount -= 0.004f;
        }
    }
}
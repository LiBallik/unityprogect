﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road : MonoBehaviour
{
    // Start is called before the first frame update
    public enum PathTypes
    {
        linear,
        loop
    }

    public PathTypes PathType;
    public int movementDitection = 1;
    public int moveingTo = 0;
    public Transform[] PathEnements;

    public void OnDrawGizmos()
    {
        if (PathEnements==null || PathEnements.Length< 2)
                {
            return;
        }
        for (int i = 1; i < PathEnements.Length; i++)
        {
            Gizmos.DrawLine(PathEnements[i - 1].position, PathEnements[i].position);
        }
        if (PathType==PathTypes.loop) {
            Gizmos.DrawLine(PathEnements[0].position, PathEnements[PathEnements.Length - 1].position);

        }
    }
    public IEnumerator<Transform> GetNextPathPoint()
    {
if (PathEnements == null || PathEnements.Length < 1)
        { yield break; }
        while (true)
        {
            yield return PathEnements[moveingTo];
            if (PathEnements.Length==1)
            { 
                continue;
            }
                
            if (PathType==PathTypes.linear)
            {
                if (moveingTo<=0)
                { 
                    movementDitection = 1;
                }
                else if (moveingTo >=PathEnements.Length-1)
                {
                    movementDitection = -1;
                }
            }
            moveingTo = moveingTo + movementDitection;
        if (PathType== PathTypes.loop)
            {
                if (moveingTo >= PathEnements.Length)
                {
                    moveingTo = 0;
                }
                else if (moveingTo <0)
                {
                    moveingTo = PathEnements.Length-1;
                }
            }

        }
    }
}

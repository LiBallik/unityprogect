﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollFloor : MonoBehaviour
{
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("Dragon"))
        {
            Invoke("FallPlatform", 0.5f);
            Destroy(gameObject,2f);
        }
    }
    void FallPlatform()
    {
        rb.isKinematic = false;
    }
}

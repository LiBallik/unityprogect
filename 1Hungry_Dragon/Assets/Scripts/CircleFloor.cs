﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleFloor : MonoBehaviour
{
    
   public Transform center;
    // Update is called once per frame
    [SerializeField]
    float radius = 2f, angularSpeed = 2f;
    float positionX, positionY, angle = 0f;
    void Update()
    {

        positionX = center.position.x + Mathf.Cos(angle) * radius;
        positionY = center.position.y + Mathf.Sin(angle) * radius;
        transform.position = new Vector2(positionX, positionY);
        angle = angle + Time.deltaTime * angularSpeed;
    
        if (angle>=360)
        {
            angle = 0;
        }
    }
}

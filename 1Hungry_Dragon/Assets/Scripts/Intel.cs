﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intel : MonoBehaviour
{

    private bool moovingLeft= true;
    public Transform groundDetection;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * 2f* Time.deltaTime);
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, 0.5f);

        if (groundInfo.collider == false)
        {
            if (moovingLeft == true)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
                moovingLeft = false;
            }
            else
            {
                transform.eulerAngles = new Vector3(0, -180, 0);
                moovingLeft = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrul : MonoBehaviour
{
    public int positionofPatrul;

    public Transform point;

    bool moveingRight= true;

    Transform player;

    public float stoppingDistance;

    public float speed;

    bool attack = false;
    bool chill = false;
    bool back=false;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Dragon").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, point.position) < positionofPatrul && attack==false)
        {
            chill = true; 
        }
        if (Vector2.Distance(transform.position, player.position) < stoppingDistance) 
        {
            attack = true;
            chill = false;
            back = false;
        }
        if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
        {
            back = true;
            attack = false;
        }
         if (chill==true)
        { 
            Chill();
        }
        else if (back == true)
        {
            GoBack();
        }
        else if (attack == true)
        {
            Attak();
        }
    }
    void Chill()
    {
        if (transform.position.x> point.position.x + positionofPatrul)
        {
            moveingRight = false;
        }
        //
        else if (transform.position.x < point.position.x - positionofPatrul)
        {
            moveingRight = true;
        }

        if (moveingRight==true)
        {
            transform.position = new Vector2(transform.position.x + speed * Time.deltaTime, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x -speed * Time.deltaTime, transform.position.y);
        }

    }
    void Attak()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
    }
    void GoBack()
    {
        transform.position = Vector2.MoveTowards(transform.position, point.position, speed * Time.deltaTime);
    }
}

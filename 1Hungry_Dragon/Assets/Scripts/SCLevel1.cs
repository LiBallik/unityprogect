﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SCLevel1 : MonoBehaviour
{
    Rigidbody2D rb;
    Animator anim;
    public GameObject btnLeft;
    public GameObject btnRight;
    public GameObject btnJump;
    public GameObject btnAttac;
    float PosBtnLeft;
    float PosBtnRight;

    float PosBtnJump;
    float PosBtnAttac;
    float run;

    Vector3 moveRight = new Vector3(0.5f, 0.5f, 0.5f);
    Vector3 moveLeft = new Vector3(-0.5f, 0.5f, 0.5f);

    Vector3 camPos = Vector3.zero;
    bool dead = false;

    public Text seed;
    public Text win;
    public Slider life;
    public GameObject stone;
    int seedAmount = 0;

    bool isGrounded = false;
    public Transform groundCheck;
    float groundRadius = 0.2f;
    public LayerMask whatIsGround;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        camPos.z = -10;

        PosBtnLeft = btnLeft.transform.position.y;
        PosBtnRight = btnRight.transform.position.y;
        PosBtnJump = btnJump.transform.position.y;
        PosBtnAttac = btnAttac.transform.position.y;
        life.value = 4;
        //seed.text = "0";
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!dead)
        {
            if (PosBtnLeft != btnLeft.transform.position.y)
            {
                run = -5f;
                transform.localScale = moveLeft;

            }
            else if (PosBtnRight != btnRight.transform.position.y)
            {
                run = 5f;
                transform.localScale = moveRight;
            }
            else run = 0f;
            rb.velocity = new Vector2(run, rb.velocity.y);

            if (PosBtnJump != btnJump.transform.position.y)
            {
                isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);

                if (isGrounded == true)
                {
                    rb.AddForce(transform.up * 2f, ForceMode2D.Impulse);
                }
            }
            if (PosBtnAttac != btnAttac.transform.position.y && seedAmount != 0)
            {
               
                    GameObject stones = Instantiate(stone, new Vector3(transform.position.x + 1.0f, transform.position.y + 0.07f, -1.2f), Quaternion.identity) as GameObject;
                    seedAmount -= 1;
                    seed.text = seedAmount.ToString();
                
            }
        }
   

        DoAnimations();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Fruit":
                {
                    Destroy(collision.gameObject);
                    seedAmount += 1;
                    seed.text = seedAmount.ToString();
                    break;
                }
            case "Mushroom":
                {
                    Destroy(collision.gameObject);
                    life.value -= 1;
                    break;
                }
            case "dieZona":
                {
                    Die();
                    break;
                }
            case "Enemy":
                {
                    life.value -= Time.deltaTime * 0.5f;
                    break;
                }
            case "win":
                {
                   win.text = "You Win!!!!!";
                    Destroy(collision.gameObject);
                    Invoke("LoadMenu", 3f);
                    break;
                }
            case "Life":
                {
                    Destroy(collision.gameObject);
                    if (life.value != 4) {  
                        life.value += 1; 
                    }
                  
                    break;
                }
        }
        if (life.value == 0)
        {
            Die();
        }

    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
            { 
            life.value -= Time.deltaTime * 0.5f;
            
            if (life.value == 0)
            {
                Die();
            }
        }

    }
    
        private void LateUpdate()
    {
        camPos.x = transform.position.x;
        Camera.main.transform.position = camPos;
    }

    void DoAnimations()
    {
        anim.SetBool("dieDragon", dead);
        anim.SetFloat("walkDragon", Mathf.Abs(run));
    }
    void Die()
    {
        dead = true;
        rb.velocity = new Vector2(0, 6);
        GetComponent<PolygonCollider2D>().enabled = false;
        Invoke("Restart",2f);
    }
    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}

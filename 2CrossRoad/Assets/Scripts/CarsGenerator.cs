﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarsGenerator : MonoBehaviour
{
    public CArsManager[] carsM;

    //Vector3 pos = new Vector3(40f, -1.3f, -40f);

    // Start is called before the first frame update
    public Transform instantiatePointPos;
    public Transform destroyPointPos;
    void Start()
    {
        StartCoroutine("SpawnVehicle");
    }
    
    private IEnumerator SpawnVehicle()
    {
        while (true)
        {
            //GetNew();
            yield return new WaitForSeconds(Random.Range(1f, 3f));
            GetNew();
            SpawnVehicle();

        }
    }
    void GetNew()
    {
        GameObject newCars;
        newCars = carsM[Random.Range(0, carsM.Length)].GetPlatform();
        newCars.transform.position = instantiatePointPos.position;
        newCars.SetActive(true);
    }
}

﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public bool dead=false;
    Rigidbody rb;
    Vector3 startPos;  Vector3 endPos;
    Vector3 endRotation;
    Vector3 moveRight = new Vector3(0, 90, 0);
    Vector3 moveLeft = new Vector3(0, -90, 0);
    Vector3 moveUp = new Vector3(0, 0, 0);
    Vector3 moveDown = new Vector3(0, -180, 0);

    Vector2 deltafinger;
    Vector2 startSwipePosition;
    Vector2 endSwipePosition;
    float swipeLength;
    
    public Text roadCheck;
    int roCheck;
    public Text coinsCheck;
    int coCheck;
    public Text resultCheck;
    Vector3 maxPos;

    public Transform instantiatePointPos;
    public Transform destroyPointPos;
    public Transform minPointPos;

    public GameObject Menu;
    public Text Recordcheck;

    // Start is called before the first frame update

    Object LoadedSave; 
    struct Object
    {
        public int saveNumer1;
        public int saveNumer2;
    }

    Animator anim;
    public AudioClip din;
    public AudioClip bulk;
    public AudioClip beep;
    public AudioClip carcar;
    //float valuecheck;

    public GameObject gameName;
    Object Save = new Object();
    public Sprite enableSprite;
    public Sprite disableSprite;
    Image image;
    public GameObject Button;
    
    void Start()
    {
      image = Button.GetComponent<Image>();

        GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("Volume");

        gameName.SetActive(true);
        Recordcheck.text = "";
        Menu.SetActive(false);
        LoadedSave = JsonUtility.FromJson<Object>(PlayerPrefs.GetString("SaveRezult"));
        coCheck = LoadedSave.saveNumer1;
        coinsCheck.text = coCheck.ToString();

        resultCheck.text = "Рекорд: "+ LoadedSave.saveNumer2.ToString();

        maxPos = transform.position;
        anim = GetComponent<Animator>();
       
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0 && !dead)
        {

            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                startSwipePosition = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                endSwipePosition = touch.position;
                swipeLength = (endSwipePosition - startSwipePosition).magnitude;
                if (swipeLength > 0.2)
                {
                    swipeControl();
                }
            }
        }
    }

    void swipeControl() 
    {
        var Twin = DOTween.Sequence();

        if (gameName.activeInHierarchy)
        {
            Twin.Append((gameName.transform.DOMove(new Vector3(gameName.transform.position.x + 15f, gameName.transform.position.y, gameName.transform.position.z), 1f, false)));
            Twin.OnComplete(Control);
            
        }
        else 
            {
            GetComponent<AudioSource>().PlayOneShot(din);
            startPos = endPos = transform.position;
                Vector2 deltafinger = endSwipePosition - startSwipePosition;

                if (Mathf.Abs(deltafinger.x) > Mathf.Abs(deltafinger.y))
                {
                    //swap to right
                    if (deltafinger.x > 0 && transform.position.x < 27.5)
                    {
                        endPos.x += 2f;
                        endRotation = moveRight;
                    }

                    //swap to left
                    if (deltafinger.x < 0 && transform.position.x > 0)
                    {
                        endPos.x -= 2f;
                        endRotation = moveLeft;
                    }
                }
                else if (Mathf.Abs(deltafinger.x) < Mathf.Abs(deltafinger.y))
                {
                    if (deltafinger.y > 0)
                    {
                        endPos.z += 2.5f;
                        endRotation = moveUp;
                        instantiatePointPos.transform.Translate(0f, 0f, 2.5f);
                        if (minPointPos.transform.position.z - destroyPointPos.position.z > 10f)
                        {
                            destroyPointPos.transform.Translate(0f, 0f, 2.5f);
                        }

                        minPointPos.transform.Translate(0f, 0f, 2.5f);
                    }


                    if (deltafinger.y < 0 && minPointPos.transform.position.z - destroyPointPos.position.z > 2.5f)
                    {
                        endPos.z -= 2.5f;
                        endRotation = moveDown;
                        instantiatePointPos.transform.Translate(0f, 0f, -2.5f);
                        //destroyPointPos.transform.Translate(0f, 0f, -2.5f);
                        minPointPos.transform.Translate(0f, 0f, -2.5f);
                    }
                }
                Twin.Append( transform.DORotate(endRotation, 0.05f, RotateMode.Fast));
                Twin.Join(transform.DOMove(endPos, 0.1f, false));
                Twin.OnComplete(RoadRecord);
            }
        
    }

    void Control() { 
        gameName.SetActive(false); 
        resultCheck.enabled = false; 
       
    }

    //
    private void RoadRecord()
    {
        if (maxPos.z < transform.position.z)
        {
            maxPos.z = transform.position.z;

            roCheck += 1;
            roadCheck.text = roCheck.ToString();
        }
    }

    //чтобы двигаться за бревнами, которые плавают
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Timber")
        {
           
            transform.parent = collision.gameObject.transform;
            
        } 
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Timber")
        {
           transform.parent = null;
           
            transform.localScale =new Vector3(100,100,100);
        }
    }

    
private void OnTriggerEnter(Collider other)
    {
        PlayerPrefs.SetFloat("Volume", 1);
        switch (other.gameObject.tag)
        {
            case "Coin": { 
                    coCheck += 1;
                    coinsCheck.text = coCheck.ToString();
                    other.GetComponent<CoinsGenerator>().coinput = true;
                    break; 
                }
            case "DeadZone": {
                    dead = true;
                    GetComponent<AudioSource>().PlayOneShot(bulk);
                    Die();
                    break; }
            case "Car":
                {
                    dead = true;
                    gameObject.transform.localScale = new Vector3( gameObject.transform.localScale.x, 10, gameObject.transform.localScale.z);
                    GetComponent<AudioSource>().PlayOneShot(beep);
                    Die();
                    break;
                }
        }
    }

     public bool bird=false;    
   public void Die()
    {
        //anim.SetBool("die", dead); на андройде все равно не работает
        if (bird == true)
        { GetComponent<AudioSource>().PlayOneShot(carcar);
        }
        Invoke("Restart", 1.5f);
        
    }


    //Если кнопка выкл.вкл нажата
public void ButtonMisic()
    {
        if (GetComponent<AudioSource>().volume == 1)
        {
            PlayerPrefs.SetFloat("Volume", 0);
            image.sprite = disableSprite;
        }
        else
        {
            PlayerPrefs.SetFloat("Volume", 1);
            image.sprite = enableSprite;
        }
    }
   

    //Сохраняем все и перезагружаем
    void Restart()
    {
        gameObject.SetActive(false);
        GetComponent<Rigidbody>().isKinematic = true;

        PlayerPrefs.SetFloat("Volume", GetComponent<AudioSource>().volume);

        if (GetComponent<AudioSource>().volume > 0)
        {
            image.sprite = enableSprite;
        }
        else
        {
            image.sprite = disableSprite;
        }
        Menu.SetActive(true);

        Save.saveNumer1 = coCheck;

        if (roCheck > LoadedSave.saveNumer2)
        {
            Save.saveNumer2 = roCheck;
            Recordcheck.text = "Новый рекорд \n" + roCheck;
        }
        else { Save.saveNumer2 = LoadedSave.saveNumer2; Recordcheck.text = "Результат \n" + roCheck;
        }
        PlayerPrefs.SetString("SaveRezult", JsonUtility.ToJson(Save));   
    }
    
}


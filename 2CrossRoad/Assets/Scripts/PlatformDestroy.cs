﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDestroy : MonoBehaviour
{
    public GameObject  destroyPointPos;
    // Start is called before the first frame update
    void Start()
    {
        destroyPointPos = GameObject.Find("destroyPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < destroyPointPos.transform.position.z)
        {
            gameObject.SetActive(false);
        }
    }
}

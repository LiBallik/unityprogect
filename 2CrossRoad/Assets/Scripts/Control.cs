﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 positions;
    public GameObject bird;
    public GameObject gameName;
   
    void Start()
    {
        positions = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2.5f);
        StartCoroutine(Co_WaitForSeconds());
    }

    private IEnumerator Co_WaitForSeconds()
    {
               
        while (GetComponent<Player>().dead == false)
        {
            yield return new WaitForSeconds(4f);
            if (positions.z == transform.position.z && !gameName.activeInHierarchy)
            {   Instantiate(bird);
                bird.transform.position = new Vector3(transform.position.x-15, transform.position.y, transform.position.z-5); 
                GetComponent<Player>().bird = true;
                GetComponent<Player>().Die();
                break;
            }
            positions = transform.position;
        }
    }
}
  
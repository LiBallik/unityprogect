﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CArsManager : MonoBehaviour
{

    public GameObject Car;
    public int CarAmount;
    
    List<GameObject> Cars;


    // Start is called before the first frame update
    void Start()
    {
        Cars = new List<GameObject>();
        for (int i = 0; i < CarAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(Car);
            obj.SetActive(false);///
                Cars.Add(obj);
        }
    }


    public GameObject GetPlatform()
    {
        for (int i = 0; i < Cars.Count; i++)
        {
            if (!Cars[i].activeInHierarchy)
            {
                    return Cars[i];
            }
        }
        GameObject obj = (GameObject)Instantiate(Car);
        obj.SetActive(false);
        Cars.Add(obj);
        return obj;
    }
}


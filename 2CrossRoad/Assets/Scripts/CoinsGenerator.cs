﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsGenerator : MonoBehaviour
{
    public bool coinput= false;
    public Transform instPointPos;
    public Transform estruyPointPos;
    Vector3 coinPos;
    // Start is called before the first frame update
    void Start()
    {
        coinPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
      if (coinput == true)
        {
            coinput = false;
            transform.DOMove(new Vector3(Random.Range(0, 14) * 2, coinPos.y, instPointPos.position.z - Random.Range(0, 16) * 2.5f - 1.5f), 0f, false);
            Invoke("Repos", 20f);
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Earth")
        {
            coinput = true;
        }
    }
    
   void Repos() { coinput = true; }
}

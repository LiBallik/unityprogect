﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    public PlatformManager[]  platformsM;
    
    Vector3 pos = new Vector3(40f, -1.3f, -40f);
    
    // Start is called before the first frame update
    public Transform instantiatePointPos;
    public Transform destroyPointPos;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < instantiatePointPos.position.z)
        {
            pos.z += 2.5f;
            transform.position = pos;
            GameObject newPlatform;
            if (transform.position.z != -22.5f)
            {
                newPlatform = platformsM[Random.Range(0, platformsM.Length)].GetPlatform();
            }
            else { 
                newPlatform = platformsM[Random.Range(0, 3)].GetPlatform();
            }
            newPlatform.transform.position = pos;
            newPlatform.SetActive(true);



        }
       
    }

}

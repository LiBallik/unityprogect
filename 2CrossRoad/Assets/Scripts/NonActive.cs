﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NonActive : MonoBehaviour
{
    public GameObject destroyPointPos;
    // Start is called before the first frame update
    void Start()
    {
        destroyPointPos = GameObject.Find("EndPoint");
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x - 1f < destroyPointPos.transform.position.x)
        {
            gameObject.SetActive(false);
        }
    }
}

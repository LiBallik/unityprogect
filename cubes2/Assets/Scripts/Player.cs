﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Player : MonoBehaviour
{
    Rigidbody rb;
    float hor, ver;
    int speed = 5;
    int score = 0;
    [SerializeField] Text scoreText;
    bool controlTrigger=false;

    public Image bar;
    [SerializeField] double fill;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>(); 
        scoreText.text = "Очки:" + score;
        fill = 1f;
    }

    // Update is called once per frame
    void Update()
    {
       
        hor = Input.GetAxis("Horizontal");
        ver = Input.GetAxis("Vertical");
        
        if (controlTrigger==false)
            fill -= Time.deltaTime*0.05;
        bar.fillAmount = (float)fill;
        if (fill < 0)
        {
            SceneManager.LoadScene("SampleScene");
        }

    }
    private void FixedUpdate()
    {
        rb.transform.Translate(Vector3.right*hor*speed*Time.fixedDeltaTime);
        rb.transform.Translate(Vector3.up*ver*speed*Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {   
        switch (collision.gameObject.tag) {
            case "Friend": { Destroy(collision.gameObject);
                    score++;
                    scoreText.text = "Очки:" + score;
                    fill += 0.1;
                    break;
                }
            case "Enemy":
                {
                    Destroy(collision.gameObject);
                    fill -= 0.3;
                    if (fill < 0)
                    { 
                        SceneManager.LoadScene("SampleScene"); 
                    }
                    break;
                }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        controlTrigger = true;
    }
    void OnTriggerStay(Collider other)
    {
        fill += Time.deltaTime * 0.05;
    }
    void OnTriggerExit(Collider other)
    {
        controlTrigger = false;
    }
}

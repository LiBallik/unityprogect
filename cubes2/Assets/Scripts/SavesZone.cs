﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavesZone : MonoBehaviour
{
    [SerializeField] GameObject SaveZone;
    float timer = 1;
    Vector3 newPos = Vector3.zero;
    int posSave=3,randPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime*0.4f;
        {
            if (timer < 0)
            {
                
                    randPosition = Random.Range(1, 5);
                
               if (randPosition != posSave)
                {
                    posSave = randPosition;
                    switch (randPosition)
                    {
                        case 1:
                            {
                                newPos.x = 8.73f;
                                newPos.y = -4.17f;
                                break;
                            }
                        case 2:
                            {
                                newPos.x = 8.788f;
                                newPos.y = 4.9f;
                                break;
                            }

                        case 3:
                            {
                                newPos.x = -8.79f;
                                newPos.y = -4.18f;
                                break;
                            }
                        case 4:
                            {
                                newPos.x = -8.66f;
                                newPos.y = 4.79f;
                                break;
                            }
                    }
                    Destroy(SaveZone, Random.Range(0, 0.2f));
                    Instantiate(SaveZone, newPos, Quaternion.Euler(0f, 0f, 45f));
                }
                timer = Random.Range(0.3f, 1.5f);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    [SerializeField] float min,max;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, Random.Range(min, max));
    }

    // Update is called once per frame
    void Update()
    {  
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class Generator : MonoBehaviour
{
    [SerializeField] GameObject friendPrefab, enemyPrefab;
    float timer = 1;
    Vector3 newPos = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    //время жизни..но в зоне сохранения не меняется..
    //зона сохранения...полупрозрачный  треугольник.. движется...
    //это через триггеры
    //гайды rollaboll;
    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        {
            if (timer < 0)
            {
                newPos.x = Random.Range(-8f, 8f);
                newPos.y = Random.Range(-5f, 5f);
                while (Vector3.Distance(newPos, transform.position) < 1.5f)
                {
                    newPos.x = Random.Range(-8f, 8f);
                    newPos.y = Random.Range(-5f, 5f);
                }
              if (Random.Range(0, 10) <5)
            {
                Instantiate(friendPrefab, newPos, Quaternion.identity);
            }
            else
            {
                Instantiate(enemyPrefab, newPos, Quaternion.identity);
            }
             timer = Random.Range(0.3f, 1.5f);
            }  
        }
    }
}

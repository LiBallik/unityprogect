﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GirlState : MonoBehaviour
{
    Rigidbody rb;
    Animator anim;
    GameObject player;
    int animation_state;
    float speed;
    Vector3 playerPos;
    bool walk = false;
    bool die = false;
    public Slider slider;
    public Text texter;
    void Start()
    {
        rb= GetComponent<Rigidbody>();

        player = GameObject.FindGameObjectWithTag("Player");
        anim = GetComponent<Animator>();
        //animation_state = Random.Range(1,2);
       speed = Random.Range(1.0f, 2.5f);
    }

    // Update is called once per frame
    void Update()
    { 
        float Distance = Vector3.Distance(transform.position, player.transform.position);
        if (Distance<15 && !walk && !die)
        {
        animation_state =  Random.Range(1, 3);
        }
        if (Distance < 10 && !walk && !die)
        {
            animation_state = 5; 

        }
        if (Distance <6 && !walk && !die)
        {
            walk = true;
            animation_state = 4;
           
        }
        if (Distance>4 & walk && !die)
        {
            animation_state = 4;
            playerPos.x = player.transform.position.x;
            playerPos.z = player.transform.position.z;
            
            transform.position = Vector3.MoveTowards(transform.position, playerPos, Time.deltaTime * 3 * speed);
            Vector3 lTargetDir = player.transform.position - transform.position;
            lTargetDir.y = 0.0f;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.time);
        }
        if (Distance < 4 && !die)
        {   animation_state = 6;
            Vector3 lTargetDir = player.transform.position - transform.position;
            lTargetDir.y = 0.0f;
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(lTargetDir), Time.time);
            slider.value -= 0.1F; 
            
        }
        if (slider.value <= 1)
        {
            die = true;
            animation_state = 2;
        }

        DoAnims();
    }
   void DoAnims()
    {
        anim.SetInteger("State", animation_state);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Player : MonoBehaviour
{
    Rigidbody rb;
    float hor, ver;
    public GameObject povorotnic;
    public GameObject umenshit;
    float size;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        size = 3.5f;
    }

    // Update is called once per frame
    void Update()
    {

        hor = Input.GetAxis("Horizontal");
        ver = Input.GetAxis("Vertical");
        if (Input.GetKey(KeyCode.Q))
        { povorotnic.transform.Rotate(new Vector3(45, -45, 0) * Time.deltaTime); }
        if (Input.GetKey(KeyCode.E))
        { povorotnic.transform.Rotate(new Vector3(45, 45, 0) * Time.deltaTime); }
        if (Input.GetKey(KeyCode.Y) && size > 0)
        {
            size = size - (size * 0.1f);
            umenshit.transform.localScale = new Vector3(size, size, 2);
        }

    }
    private void FixedUpdate()
    {
        rb.transform.Translate(Vector3.right*hor*5*Time.fixedDeltaTime);
        rb.transform.Translate(Vector3.up*ver*5*Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.tag) {
            case "Cube4": { 
        Destroy(collision.gameObject);
                    break;
                }
        }
    }
    private void OnTriggerStay(Collider other)
    {
       if(other.gameObject.tag=="Cube5" && (Input.GetKeyDown(KeyCode.Tab)))
                    {
      Destroy(other.gameObject); 
        }
    }
}

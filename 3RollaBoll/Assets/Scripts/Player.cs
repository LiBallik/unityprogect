﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Rigidbody rb;
    public float speed;
     int score = 0;
    [SerializeField] Text scoreText;
    [SerializeField] Text winText;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);
    }
    private void OnTriggerEnter(Collider collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Picks":
                {
                    Destroy(collision.gameObject);
                    score++;
                    if (score != 14)
                    { 
                        scoreText.text = "Очки:" + score; 
                    }
                    else {
                        winText.text = "Вы победили!!!";
                        scoreText.text = " ";
                            SceneManager.LoadScene("SampleScene");
                    }
                        break;
                }
        }
    }
}


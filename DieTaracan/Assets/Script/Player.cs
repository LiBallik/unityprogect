﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    Rigidbody2D rb;
    Animator anim;
    float hor, vert;
    public Texture2D cursor;
    public int hp = 10;
 
    void Start()
    {   
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
        rb= GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    
        
    }

    // Update is called once per frame
    void Update()
    {
        hor = Input.GetAxis("Horizontal");
        vert = Input.GetAxis("Vertical");
        anim.SetFloat("run", System.Math.Abs(hor) + System.Math.Abs(vert));
        Vector3 mousepos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
        Vector3 diff = mousepos - transform.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rot_z - 90), Time.fixedDeltaTime * 4);
        Life();
    }

    private void LateUpdate()
    {
        Camera.main.transform.position = new Vector3(transform.position.x, transform.position.y, -10);
    }
    private void FixedUpdate()
    {
        rb.transform.Translate(Vector2.right * Time.fixedDeltaTime * 5 * hor, Space.World);
        rb.transform.Translate(Vector2.up * Time.fixedDeltaTime * 5 * vert, Space.World);
    
    }
    private void Life() 
    {
        if (hp <= 0)
        {
           GetComponent<Player>().enabled = false;
            GetComponent<GunGun>().enabled = false;
            SceneManager.LoadScene("SampleScene");
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunGun : MonoBehaviour
{
    public GameObject gun, bullet;
    [SerializeField] float fireRater  =0.1f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        fireRater -= Time.deltaTime;
        if (Input.GetMouseButton(0) && fireRater < 0)
        { 
            shutBullet(); 
        }
    }
    public void shutBullet() 
    {
        Instantiate(bullet, gun.transform.position, gun.transform.rotation);
        fireRater = 0.1f;
    }
}

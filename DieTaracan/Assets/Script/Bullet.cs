﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject blood;
    public GameObject[] bloods;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject,4);
        rb.transform.Rotate(Vector3.forward*Random.Range(-5,5));
        rb.AddRelativeForce(Vector2.up * 20,ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}

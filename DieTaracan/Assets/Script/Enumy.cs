﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enumy : MonoBehaviour
{
    public GameObject blood;
    public GameObject[] bloods;
    Rigidbody2D rb;
    Animator anim;
    public GameObject Player;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        Player = GameObject.FindGameObjectWithTag("Player");
    }


    void Update()
    {

    }
    private void FixedUpdate()
    {
        rb.transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, Time.fixedDeltaTime * 7);
        Vector3 playerPos = Player.transform.position;
        Vector3 diff = playerPos - transform.position;
        diff.Normalize();
        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, rot_z - 90), Time.fixedDeltaTime * 3);
        // Quaternion- вращение
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "bullet":
                {

                    //Destroy(gameObject);
                    GetComponent<Collider2D>().enabled = false;
                    GetComponent<Animator>().enabled = false;
                    GetComponent<Animator>().speed = 0;
                    gameObject.isStatic = true;
                    GetComponent<Enumy>().enabled = false;
                    GameObject bloodnew = Instantiate(blood, transform.position, transform.rotation) as GameObject;
                    Instantiate(bloods[Random.Range(0, bloods.Length)], transform.position, transform.rotation);
                    Destroy(bloodnew, 0.3f);
                    Destroy(collision.gameObject);
                    break;
                }

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "Player":
                {
                    collision.gameObject.GetComponent<Player>().hp-=1;
                   
                    break;
                }
        }
    }
}

